﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    private mainscript testScripts;
    private Rigidbody2D rb;

    void Start()
    {
        script = GameObject.Find("testScripts").GetComponent<mainscript>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-100 * Time.deltaTime, 0);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "player1")
        {
            rb.velocity = new Vector2(100 * Time.deltaTime, gameObject.transform.position.y * 2 - col.transform.position.y * 2);
        }
        if (col.gameObject.name == "player2")
        {
            rb.velocity = new Vector2(-100 * Time.deltaTime, gameObject.transform.position.y * 2 - col.transform.position.y * 2);
        }
        if (col.gameObject.name =="left border")
        {
            testScripts.lefthit();
        }
        if (col.gameObject.name == "right border")
        {
            testScripts.righthit();
        }
    }

    void Update ()
    {
        if (player == player1)
        {
            string data = "ball" + ":" + ball.transform.position.x + ":" + ball.transform.position.y;
            byte[] bytedata = System.Text.ASCIIEncoding.Default.GetBytes(data);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, bytedata);

            string player1data = "player1" + ":" + player1.transform.position.x + ":" + player1.transform.position.y;
            byte[] player1dataarray = System.Text.ASCIIEncoding.Default.GetBytes(player1data);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, player1dataarray);
        }
        else if (player == player2)
        {
            string player2data = "player2" + ":" + player2.transform.position.x + ":" + player2.transform.position.y;
            byte[] player2dataarray = System.Text.ASCIIEncoding.Default.GetBytes(player2data);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, player2dataarray);
        }
        else { }
    }
}

/*public class Ball : MonoBehaviour
{

    [SerializeField]
    float speed;
    float radius;
    Vector2 direction;

    // Start is called before the first frame update
    void Start()
    {
        direction = Vector2.one.normalized;
        radius = transform.localScale.x / 2;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * speed * Time.deltaTime);

        if (transform.position.y < GameManager.bottomLeft.y + radius && direction.y < 0)
        {
            direction.y = -direction.y;
        }

        if (transform.position.y > GameManager.topRight.y - radius && direction.y > 0)
        {
            direction.y = -direction.y;
        }

        //game over
        if (transform.position.x < GameManager.bottomLeft.x + radius && direction.x < 0)
        {
            Debug.Log("Right Player Wins!!!");

            Time.timeScale = 0;
            enabled = false;
        }

        if (transform.position.x > GameManager.topRight.x - radius && direction.x > 0)
        {
            Debug.Log("Left Player Wins!!!");

            Time.timeScale = 0;
            enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Paddle")
        {
            bool isRight = other.GetComponent<Paddle>().isRight;

            if (isRight == true && direction.x > 0)
            {
                direction.x = -direction.x;
            }

            if (isRight  == false && direction.x < 0)
            {
                direction.x = -direction.x;
            }
        }
    }
}*/
