﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.SocialPlatforms;

public class testScripts : MonoBehaviour, RealTimeMultiplayerListener
{
    public GameObject signin;
    public GameObject randomplayers;
    public GameObject friends;
    //public GameObject maincanvas;
    //public GameObject playcanvas;
    public GameObject signincanvas;
    public GameObject GameManager;

    public Text loading;

    public GameObject win;
    public GameObject lose;

    public GameObject player1;
    public Text playerText1;
    public GameObject player2;
    public Text playerText2;

    public GameObject player;
    public GameObject ball;

    // Start is called before the first frame update
    void Start()
    {
        ball.SetActive(false);

        win.SetActive(false);
        lose.SetActive(false);

        playerText1.text = "";
        playerText2.text = "";

        loading.enabled = false;   
        signin.SetActive(true);
        randomplayers.SetActive(false);
        friends.SetActive(false);
        //maincanvas.SetActive(true);
        //playcanvas.SetActive(false);
        signincanvas.SetActive(true);
        GameManager.SetActive(false);


        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate((bool success) =>
        {
            if (success == true)
            {
                signin.SetActive(false);
                randomplayers.SetActive(true);
                friends.SetActive(true);
            }
            else
            {
                signin.SetActive(true);
                randomplayers.SetActive(false);
                friends.SetActive(false);
            }
        });
    }

    public void signinPress()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success == true)
            {
                signin.SetActive(false);
                randomplayers.SetActive(true);
                friends.SetActive(true);
            }
            else
            {
                signin.SetActive(true);
                randomplayers.SetActive(false);
                friends.SetActive(false);
            }
        });

    }

    public void CreateQuickGames()
    {
        signincanvas.SetActive(false);
        GameManager.SetActive(true);
        loading.enabled = true;

        const int MinOpponents = 1, MaxOpponents = 1;
        const int GameType = 0;

        PlayGamesPlatform.Instance.RealTime.CreateQuickGames(MinOpponents, MaxOpponents, GameType, this);
    }

    public void CreateWithInvitationScreen()
    {
        signincanvas.SetActive(false);
        GameManager.SetActive(true);
        loading.enabled = true;

        const int MinOpponents = 1, MaxOpponents = 1;
        const int GameType = 0;

        PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(MinOpponents, MaxOpponents, GameType, this);
    }

    public void mainmenupress()
    {
        signin.SetActive(false);
        randomplayers.SetActive(true);
        friends.SetActive(true);
        signincanvas.SetActive(true);
        GameManager.SetActive(false);
        //maincanvas.SetActive(true);
        //playcanvas.SetActive(false);
    }

    public void onPeersConnected(string[] participantId)
    {
        throw new NotImplementedException();
    }

    //public void onPeersConnected(string[] participantId)
    //{
    //    //mainmenupress();
    //    throw new NotImplementedException();
    //}

    public void OnParticipantLeft(Participant participant)
    {
        //throw new NotImplementedException();
        mainmenupress();
    }

    public void onPeersDisconnected(string[] participantId)
    {
        mainmenupress();
    }

    public void onLeftRoom()
    {
        mainmenupress();
    }

    public void onRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
        string position = System.Text.Encoding.Default.GetString(data);
        string[] raw = position.Split(new string[] { ":" }, System.StringSplitOptions.RemoveEmptyEntries);
        if (!isReliable)
        {
            if (raw[0] == "ball")
            {
                //raw1 x, raw2 y
                ball.transform.position = new Vector2(System.Convert.ToSingle(raw[1]), System.Convert.ToSingle(raw[2]));
            }
            if (raw[0] == "player1")
            {
                player1.transform.position = new Vector2(System.Convert.ToSingle(raw[1]), System.Convert.ToSingle(raw[2]));
            }
            if (raw[0] == "player2")
            {
                player2.transform.position = new Vector2(System.Convert.ToSingle(raw[1]), System.Convert.ToSingle(raw[2]));
            }
        }
        else
        {
            if (raw[0] == "0win")
            {
                righhit();
            }
            if (raw[0] == "0lose")
            {
                lefthit();
            }
        }
        //throw new NotImplementedException();
    }

    public void onRoomSetupProgress(float progress)
    {
        loading.text = progress.ToString();
    }

    public void onRoomConnected(bool success)
    {
        loading.text = "";
        Participant myself = PlayGamesPlatform.Instance.RealTime.GetSelf();
        List<Participant> participants = PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();
        playerText1.text = participants[0].DisplayName;
        playerText2.text = participants[1].DisplayName;
        ball.SetActive(true);
        if (myself.ParticipantId == participants[0].ParticipantId)
        {
            player = player1;
            ball.GetComponent<Ball>().enabled = true;
        }
        else if (myself.ParticipantId == participants[1].ParticipantId)
        {
            player = player2;
            ball.GetComponent<Ball>().enabled = false;
        }
        else { 
        }

    }

    public void lefthit()
    {
        if (player == player1)
        {
            win.SetActive(false);
            lose.SetActive(true);

            string lost = "0lose";
            byte[] lostarray = System.Text.ASCIIEncoding.Default.GetBytes(lost);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(true, lostarray);
        }
        else
        {
            win.SetActive(true);
            lose.SetActive(false);
        }
    }

    public void righthit()
    {
        if (player == player1)
        {
            win.SetActive(true);
            lose.SetActive(false);
        }
        else
        {
            win.SetActive(false);
            lose.SetActive(true);
        }
    }

    public void up()
    {
        player.GerComponent<Rigidbody2D>().velocity = new Vector2(0, 150 * Time.deltaTime);
    }

    public void down()
    {
        player.GerComponent<Rigidbody2D>().velocity = new Vector2(0, -150 * Time.deltaTime);
    }

    public void velocityzero()
    {
        player.GerComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
